package GA;

public class GA {
	
	private Population[] population;
	private static final int TARGET = 20;
	
	public GA(int popSize, int dnaSize){
		population = new Population[popSize];
		for(int i=0; i<popSize; i++){
			population[i] = new Population(dnaSize);
			population[i].setFitness(TARGET);
			population[i].print();
		}
	}
	
}
