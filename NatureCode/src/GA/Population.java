package GA;

public class Population {
	private String dna;
	private int sum;
	private float fitness;
	
	public Population (int dnaLength){
		dna = "";
		for(int i=0; i<dnaLength; i++){
			dna +=  (int)(Math.random()*10);
		}
	}
	
	public void print(){
		System.out.println(  dna + " : " + sum + " : " +fitness);
	}

	public String getDna() {
		return dna;
	}

	public float getFitness() {
		return fitness;
	}

	public void setFitness(int target) {
		
		for(int i=0; i<dna.length(); i++){
			sum += Integer.parseInt( dna.charAt(i)+"" );
		}
		fitness = - Math.abs( target - sum );
	}
}
